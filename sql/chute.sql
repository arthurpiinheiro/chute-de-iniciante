-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22-Jan-2018 às 14:11
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chute`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `doacao`
--

CREATE TABLE `doacao` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `numeroCartao` varchar(100) NOT NULL,
  `expiracao` varchar(7) NOT NULL,
  `cvn` varchar(3) NOT NULL,
  `cep` varchar(100) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `numero` varchar(100) NOT NULL,
  `complemento` varchar(100) NOT NULL,
  `valor` varchar(100) NOT NULL,
  `projeto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `doacao`
--

INSERT INTO `doacao` (`id`, `nome`, `numeroCartao`, `expiracao`, `cvn`, `cep`, `endereco`, `numero`, `complemento`, `valor`, `projeto_id`) VALUES
(1, 'assaasasas', '11212121212', '/', '123', '121212sa', 'sasaasas', '1221', '121212', '500,00', 38),
(2, 'assaasasas', '11212121212', '01/2016', '123', '121212sa', 'sasaasas', '1221', '121212', '500,00', 38),
(3, 'assaasasas', '11212121212', '01/2016', '123', '121212sa', 'sasaasas', '1221', '121212', '400,00', 38),
(4, 'assaasasas', '11212121212', '01/2016', '123', '121212sa', 'sasaasas', '1221', '121212', '400,00', 38);

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

CREATE TABLE `projeto` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `imagem` varchar(100) NOT NULL,
  `categoria` varchar(100) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `descricaoBreve` text NOT NULL,
  `video` varchar(100) NOT NULL,
  `dataLimite` date NOT NULL,
  `capitalLimite` varchar(100) NOT NULL,
  `descricao` longtext NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`id`, `nome`, `imagem`, `categoria`, `pais`, `descricaoBreve`, `video`, `dataLimite`, `capitalLimite`, `descricao`, `usuario_id`) VALUES
(38, 'Arthurrrrrrrrr quer anunciar na testa', 'a921db344bb8d3e635444931ef363f645a65e03476ee6.jpg', 'Viagem, Pobre', 'Brasil', 'oi, oi,oi ', 'c1f97709d76d12f9f276c9c5d48f97cd5a65e03476afe.mp4', '0000-00-00', '121212', 'saaaaaaaaaaasssssssssssassssssssssssssssssssss', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `nomeConta` varchar(100) NOT NULL,
  `foto` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `email`, `senha`, `nome`, `nomeConta`, `foto`) VALUES
(1, 'let@let.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Let', 'let', '64805e9e574781bf075e937e0f583e855a5f8c220d707.jpg'),
(3, 'let@let.com', '81dc9bdb52d04dc20036dbd8313ed055', 'leticia', 'leticia', '9a6cad017708649fbc55d0411ec0b1655a5f8c627751c.jpg'),
(4, 'let@let.com', '81dc9bdb52d04dc20036dbd8313ed055', 'leticia2', 'leticia2', '914d72d336daae9a7a1a1dbf51a256ff5a5f8c7578ce3.jpg'),
(5, 'arthur@tutu.com', '81dc9bdb52d04dc20036dbd8313ed055', 'arthur', 'arthur', '551ea5771a30f839914b078313ed76455a5f8c9181bdf.jpg'),
(6, 'tutu@tutu.com', '81dc9bdb52d04dc20036dbd8313ed055', 'tutu', 'tutu', 'a6425f588fe8480f13781b3e16ade52d5a5fa386891a2.jpg'),
(7, 'monstro@monstro.com', '81dc9bdb52d04dc20036dbd8313ed055', 'monstro', 'monstro', 'bd8cfc5dcbe77ba2b5aef831276857795a5fcef359db6.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doacao`
--
ALTER TABLE `doacao`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_doacao_projeto1_idx` (`projeto_id`);

--
-- Indexes for table `projeto`
--
ALTER TABLE `projeto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_projeto_usuario1_idx` (`usuario_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nomeConta` (`nomeConta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doacao`
--
ALTER TABLE `doacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `projeto`
--
ALTER TABLE `projeto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `doacao`
--
ALTER TABLE `doacao`
  ADD CONSTRAINT `fk_doacao_projeto1` FOREIGN KEY (`projeto_id`) REFERENCES `projeto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `projeto`
--
ALTER TABLE `projeto`
  ADD CONSTRAINT `fk_projeto_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
